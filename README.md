[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# TTRSS Purge unused account script

## What does this script do?

This script send 3 emails to Tiny Tiny RSS users which didn't connect in a given delay, then, if they still not connect, these unused accounts are deleted and a final mail is send.

Each mail comes with an OPML export of the account.

Note that this script works only on TTRSS with PostgreSQL. MySQL is unsupported.

## License

This program is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

See [LICENSE file](LICENSE).

## Installation

See [INSTALL.md](INSTALL.md) file.

## Usage

If you installed this script with `Carton`:

```
carton exec "./purge.pl --help"
```

If you installed the dependencies yourself:

```
./purge.pl --help
```
