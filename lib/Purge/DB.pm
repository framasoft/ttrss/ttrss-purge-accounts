# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package Purge::DB;

use ORLite {
    file         => 'purge.db',
    unicode      => 1,
    create       => sub {
        my $dbh = shift;
        $dbh->do(
            'CREATE TABLE users (
            id TEXT,
            login TEXT NOT NULL,
            email TEXT NOT NULL,
            strike INTEGER,
            last_strike_at INTEGER,
            instance TEXT NOT NULL)'
        );
        return 1;
    },
};

1;
