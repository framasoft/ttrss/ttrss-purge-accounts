# Installation

You will need Carton, which is a Perl dependencies manager.
It will get what you need, so don't bother for dependencies (but you can install the dependencies listed in the file `cpanfile` if you want).

```
sudo cpan Carton
```

Please, note that you may need some packages to compile the dependancies (in Debian, you'll need `build-essential` and `libpq-dev`).

After installing Carton :

```
git clone https://git.framasoft.org/framasoft/ttrss-purge-accounts.git
cd ttrss-purge-accounts
carton install
```

Then you need to edit `purge.pl` to set `MAIL_FROM` and `DEBUG_MAIL`.

That's all ;-)
