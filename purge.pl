#!/usr/bin/perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use warnings;
use strict;
use 5.10.0;
#use utf8;

use Getopt::Long;
use FindBin qw($Bin);
use lib "$Bin/lib";
use Mojo::Pg;
use File::Temp qw(tempfile);
use File::Copy qw(move);
use XML::OPML::SimpleGen;
use Purge::DB;
use DateTime;
use DateTime::Duration;
use DateTime::Format::Pg;
use Term::ProgressBar;
use MIME::Lite;

use constant {
    MAIL_FROM            => 'tech-sys@framalistes.org',
    DEBUG_MAIL           => 'luc@framasoft.org',
    WARN_EMAIL_SUBJECT   => 'A propos de votre compte Framanews',
    WARN_EMAIL_CONTENT   => "Bonjour,\n\nCela fait plus de 3 mois que vous ne vous êtes pas connecté sur votre compte\nFramanews (exactement 3 mois et XXXX jours).\nNous vous remercions de l'intérêt porté à Framanews, mais nous avons besoin de\nsupprimer les comptes inactifs afin de permettre à d'autres personnes de profiter\ndu service.\nVous recevrez 3 emails d'avertissements, après quoi votre compte sera supprimé\n(vous trouverez ci-joint un export OPML de vos flux, que vous pourrez importer\ndans à peu près n'importe quel lecteur de flux RSS).\n\nPour éviter la suppression de votre compte, il vous suffit de vous connecter à\nvotre compte sur https://framanews.org.\n\nSi vous souhaitez supprimer votre compte, ne vous connectez pas, laissez simplement passer les emails d'avertissement.\n\n-- \nL'équipe Framanews, pour Framasoft",
    DELETE_EMAIL_SUBJECT => 'Suppression de votre compte Framanews',
    DELETE_EMAIL_CONTENT => "Bonjour,\n\nSuite aux trois emails que nous vous avons envoyé, vous ne vous êtes toujours pas\nconnecté sur votre compte Framanews (depuis XXXX jours).\nCelui-ci a donc été supprimé.\n\nVous trouverez en pièce jointe de cet email un export OPML de vos flux.\n\n-- \nL'équipe Framanews, pour Framasoft",
};

my ($base, $user, $pwd, $delay, $debug, $quiet, $help);
GetOptions ('base=s'  => \$base,
            'user=s'  => \$user,
            'pwd=s'   => \$pwd,
            'delay=i' => \$delay,
            'debug'   => \$debug,
            'quiet'   => \$quiet,
            'help'    => \$help);

_help() if ($help);

mkdir "$Bin/$base" unless (-d "$Bin/$base");
my $time = time();
my $now  = DateTime->from_epoch(epoch => $time);
my $dur  = DateTime::Duration->new(months => $delay);
# $delay months ago
my $dt   = $now->subtract_duration($dur);

# database connection
my $pg = Mojo::Pg->new("postgresql://$user:$pwd\@localhost/$base");
my $db = $pg->db;

# Get TTRSS schema version
my $schema_version = $db->query('SELECT schema_version FROM ttrss_version')->hashes->first->{schema_version};

# Get unused accounts
my $users = $db->query('SELECT id, login, email, last_login FROM ttrss_users WHERE access_level = 0 AND last_login < (?)', DateTime::Format::Pg->format_datetime($dt))->hashes;

say "Nombre d'utilisateurs ne s'étant pas logués depuis plus de $delay mois : ".$users->size unless $quiet;

my $bar = Term::ProgressBar->new({ name => "Envoi des mails", count => $users->size}) unless $quiet;

# This is the real deal
$users->each(sub {
    my ($e, $num) = @_;

    $bar->update($num) unless $quiet;

    # Is the user in the purge db?
    my $user;
    my @u = Purge::DB::Users->select('WHERE id = ? AND instance = ?', $e->{id}, $base);
    unless (@u) {
        Purge::DB::Users->create(
            id             => $e->{id},
            login          => $e->{login},
            email          => $e->{email},
            strike         => 0,
            last_strike_at => 0,
            instance       => $base
        );
        @u = Purge::DB::Users->select('WHERE id = ? AND instance = ?', $e->{id}, $base);
    }
    $user = $u[0];

    if ($user->last_strike_at <= $time - 86400) {

        # OPML generation
        my $opml = new XML::OPML::SimpleGen();

        # Empty templates for OPML outlines and groups
        $opml->outline({});
        $opml->group({});

        $opml->head(
            title       => 'Framanews Feed Export',
        );

        # Get the categories
        $db->query('SELECT title, id FROM ttrss_feed_categories WHERE owner_uid = ?', $e->{id})->hashes->each(sub {
            my ($cat, $num) = @_;

            # For each category, get the feeds
            $db->query('SELECT title, feed_url, site_url FROM ttrss_feeds WHERE cat_id = ? and owner_uid = ?', ($cat->{id}, $e->{id}))->hashes->each(sub {
                my ($feed, $num) = @_;
                $opml->insert_outline(
                    group   => $cat->{title},
                    text    => $feed->{title},
                    htmlUrl => $feed->{site_url},
                    xmlUrl  => $feed->{feed_url},
                    type    => 'rss'
                );
            });
        });

        # Get the feeds without categories
        $db->query('SELECT title, feed_url, site_url FROM ttrss_feeds WHERE cat_id IS NULL and owner_uid = ?', $e->{id})->hashes->each(sub {
            my ($feed, $num) = @_;
            $opml->insert_outline(
                text    => $feed->{title},
                htmlUrl => $feed->{site_url},
                xmlUrl  => $feed->{feed_url},
                type    => 'rss'
            );
        });

        # Save the TTRSS preferences
        $opml->add_group(
            text             => 'tt-rss-prefs',
            'schema-version' => $schema_version,
        );
        $db->query('SELECT pref_name, value FROM ttrss_user_prefs WHERE profile IS NULL AND owner_uid = ? ORDER BY pref_name', $e->{id})->hashes->each(sub {
            my ($pref, $num) = @_;
            $opml->insert_outline(
                group       => 'tt-rss-prefs',
                'pref-name' => $pref->{pref_name},
                value       => $pref->{value}
            );
        });

        # Let's make the OPML like the TTRSS export
        $opml = $opml->as_string;
        $opml =~ s/ id="\d+"//gm;
        $opml =~ s/([^>])\n\s*/$1 /gms;

        # Don't know why MIME::Lite doesn't like attaching the OPML as Data :-(
        my ($fh, $tmpfile) = tempfile();
        binmode($fh, ":utf8");
        print $fh $opml;
        close $fh;

        # How many days have past from the last connection?
        my $dt2  = DateTime::Format::Pg->parse_datetime($e->{last_login});
        my $last = $dt->delta_days($dt2)->in_units('days');

        my $to   = ($debug) ? DEBUG_MAIL : $e->{email};
        my $msg;

        # You haven't been warned three times, so you can still save your account
        if ($user->strike < 3) {
            my $data = WARN_EMAIL_CONTENT;
            $data    =~ s/XXXX/$last/;
            $msg     = MIME::Lite->new(
                Encoding => 'quoted-printable',
                From     => MAIL_FROM,
                To       => $to,
                Subject  => WARN_EMAIL_SUBJECT,
                Data     => $data
            );

            # The account deletion is coming closer
            $user->update(
                strike         => $user->strike + 1,
                last_strike_at => $time,
            );
        } else {
            # You're screwed, your account is going to be deleted

            # Prepare the mail
            my $data = DELETE_EMAIL_CONTENT;
               $data =~ s/XXXX/$last/;
            $msg     = MIME::Lite->new(
                Encoding => 'quoted-printable',
                From     => MAIL_FROM,
                To       => $to,
                Subject  => DELETE_EMAIL_SUBJECT,
                Data     => $data,
            );

            # And finally, delete the user from the TTRSS database
            unless ($debug) {
                $user->delete();

                $db->query('DELETE FROM ttrss_tags WHERE owner_uid = ?', $e->{id});
                $db->query('DELETE FROM ttrss_feeds WHERE owner_uid = ?', $e->{id});
                $db->query('DELETE FROM ttrss_users WHERE id = ?', $e->{id});
            }
        }

        $msg->attr('content-type.charset' => 'UTF-8');

        # Attach OPML to the mail
        $msg->attach(
            Type     => 'application/xml+opml',
            Filename => 'export_framanews.opml',
            Path     => $tmpfile,
        );

        # Send mail
        $msg->send;

        move $tmpfile, "$Bin/$base/".$e->{login}.".opml";

        exit if ($debug);
    }
});

# Delete accounts that logged in again after receiving a mail
$db->query('SELECT id FROM ttrss_users WHERE access_level = 0 AND last_login > (?)', DateTime::Format::Pg->format_datetime($dt))->hashes->each(sub {
    my ($e, $num) = @_;
    Purge::DB::Users->delete_where('id = ? AND instance = ?', $e->{id}, $base);
});

sub _help {
    print <<EOF;
purge.pl v0.1

This script send 3 emails to TTRSS users which didn't connect in a given delay, then, if they still not connect, these unused accounts are deleted and a final mail is send.
Each mail comes with an OPML export of the account.

You need to set MAIL_FROM in the script to whatever mail address you want to use to send mail from.

(c) 2016 Framasoft.
This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

Usage   : ./purge.pl [options]
Options :
    --help                      prints this message and exit
    --debug                     run only one iteration of the loop on unused accounts and send mail to DEBUG_MAIL (you have to change it in the script)
                                it doesn't delete any account
    --quiet                     disable verbose output (useful for cron)
    --base string               the database name to connect to
    --user string               the database's user
    --pwd  string               the database's user's password
    --delay integer             number of months without login to consider an account unused
EOF
    exit 0;
}
